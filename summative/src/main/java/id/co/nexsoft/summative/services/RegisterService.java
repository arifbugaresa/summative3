package id.co.nexsoft.summative.services;

import id.co.nexsoft.summative.entities.User;
import id.co.nexsoft.summative.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

import static java.util.Objects.isNull;

@Service
public class RegisterService {

    @Autowired
    private UserRepository userRepository;

    public void register(User user) {

        /* validate null date birthdate */
        Date birthdayDate = user.getBirthDate();
        if (isNull(birthdayDate)){
            String sDate1="01/01/2010";
            try{
                Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
                user.setBirthDate(date1);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }

        userRepository.save(user);
    }

    public boolean checkRetypePassword(User user) {
        String inputPassword = user.getPassword();
        String inputRetypePassword = user.getRetypePassword();

        if (inputPassword.equals(inputRetypePassword))
            return true;
        else return false;
    }

}
