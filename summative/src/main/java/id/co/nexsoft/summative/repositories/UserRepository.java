package id.co.nexsoft.summative.repositories;

import id.co.nexsoft.summative.entities.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
    public User findByEmailAndAndPassword(String email, String password);
}
