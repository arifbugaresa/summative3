package id.co.nexsoft.summative.services;

import id.co.nexsoft.summative.entities.User;
import id.co.nexsoft.summative.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User findUser(String email, String password) {
        return userRepository.findByEmailAndAndPassword(email, password);
    }

}
