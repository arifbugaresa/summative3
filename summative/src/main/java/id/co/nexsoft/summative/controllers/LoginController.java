package id.co.nexsoft.summative.controllers;
import id.co.nexsoft.summative.entities.Login;
import id.co.nexsoft.summative.entities.User;
import id.co.nexsoft.summative.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLogin() {
        return "login.jsp";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@ModelAttribute Login login, Model model) {
        String email = login.getEmail();
        String password = login.getPassword();
        Optional<User> userRepo = Optional.ofNullable(userService.findUser(email,password));

        if (userRepo.isPresent()) {
            return "welcome.jsp";
        } else {
            model.addAttribute("message", "email atau password salah");
            return "login.jsp";
        }
    }

}
