<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 21/08/2021
  Time: 14:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Landing Page</title>

    <!-- css -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

</head>

<body>
<div class="container">

    <%--error message--%>
    <c:if test = "${message != null}">
        <div class="alert alert-danger mt-2" role="alert">
            ${message}
        </div>
    </c:if>


    <div class="mb-4 mt-4">
        <h2 class="text-center">Login</h2>
    </div>
    <div class="col-8 offset-sm-2">
        <form method="POST" modelAttribute = "login" action="/login">
            <div class="form-group row">
                <label for="email" class="col-sm-3 col-form-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="email" name="email">
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-sm-3 col-form-label">Password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="password" name="password">
                </div>
            </div>

            <button type="submit" class="btn btn-primary offset-sm-3">Sign In</button>
            <button type="button" class="btn btn-secondary" onClick="clearAllInput();">Reset</button>
        </form>

        <div class=" offset-sm-11">
            <a href="http://localhost:8080/register">Register</a>
        </div>
    </div>
</div>

<script>

    $('.alert').alert();

    function clearAllInput() {
        document.getElementById("email").value = '';
        document.getElementById("password").value = '';
    }
</script>

</body>

</html>